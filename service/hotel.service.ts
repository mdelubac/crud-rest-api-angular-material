import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Hotel } from '../model/hotel';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/internal/operators/tap';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  
  constructor(private http : HttpClient) { }

// permet de faire un refresh de la table
  private _refreshNeeded$ = new Subject<void>();

// un get pour être utilisé dans le component.ts
  get refreshNeeded$(){
    return this._refreshNeeded$;

  }
  baseUrl = 'http://localhost:8080/HotelRest';

  getHotels(): Observable<Hotel[]>{
    return this.http.get<Hotel[]>(`${this.baseUrl}/getAllHotels`)
  }
// permet de faire un refresh de la table après un save
  saveHotel(hotel: Hotel): Observable<Hotel>{
    return this.http.post<Hotel>(`${this.baseUrl}/ajout`, hotel)
    .pipe(
      tap(()=>{
        this._refreshNeeded$.next();
      })
    );
  }
// permet de faire un refresh de la table après un delete
  delete(id : number) : Observable<{}>{
    const url = `${this.baseUrl}/supprimer/${id}`;
    return this.http.delete(url)
    .pipe(
      tap(()=>{
        this._refreshNeeded$.next();
      })
    );
  }
}
