import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelComponent } from './hotel/hotel.component';
import { VoyageurComponent } from './voyageur/voyageur.component';

const routes: Routes = [
  {path:'hotel', component:HotelComponent},
  {path:'voyageur', component:VoyageurComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
