import { Component, OnInit } from '@angular/core';
import { HotelService } from '../service/hotel.service';
import { VoyageurService } from '../service/voyageur.service';

@Component({
  selector: 'app-voyageur',
  templateUrl: './voyageur.component.html',
  styleUrls: ['./voyageur.component.css']
})
export class VoyageurComponent implements OnInit {

  constructor(private voySer : VoyageurService) { }

  ngOnInit(): void {
  }

}
