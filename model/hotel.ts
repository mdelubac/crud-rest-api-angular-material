export interface Hotel {
     idHotel : number;
     nomHotel: String;
     adresseHotel : String;
     nbrEtoiles : number;
     prixNuit : number;
}
