export interface Voyageur {
    idVoyageur : number;
    nomVoyageur : String;
    prenomVoyageur :String;
    adresseVoyageur : String;
}

