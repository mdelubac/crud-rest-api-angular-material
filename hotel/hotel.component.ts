import { AfterViewInit, Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Hotel } from '../model/hotel';
import { HotelService } from '../service/hotel.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { switchMap } from 'rxjs/operators'
import { MatPaginator } from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';





@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

  hotelForm = new FormGroup({
    idHotel: new FormControl(''),
    nomHotel: new FormControl(''),
    adresseHotel :new FormControl(''),
    nbrEtoiles :new FormControl(''),
    prixNuit :new FormControl('')
  });

  hotel: Hotel;
  hotels : Hotel[];
  displayedColumns: string[] = ['id', 'nom', 'adresse', 'etoiles', 'prix', 'action'];
  dataSource = new MatTableDataSource<Hotel>();
  

  constructor(private hotSer: HotelService) { }

  ngOnInit():void {
      this.hotSer.refreshNeeded$
      .subscribe(() =>{
        this.getAllHotels();
      })

      this.getAllHotels();


  }


  getAllHotels(){
     this.hotSer.getHotels().subscribe((data)=>{this.hotels=data;

  });
  
  }

  ajouter(){
    this.hotel=this.hotelForm.value;
    this.hotSer.saveHotel(this.hotel).subscribe(()=> {
    });
    
  }

  supprimer(id:number): void{
    this.hotSer.delete(id).subscribe(()=>{

    });
    this.getAllHotels;
     

  }

}

